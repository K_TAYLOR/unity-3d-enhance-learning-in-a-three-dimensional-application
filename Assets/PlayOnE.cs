﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayOnE : MonoBehaviour {

    public AudioSource NoteSound;

	// Use this for initialization
	void Start () {
        NoteSound = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonUp("e"))
        {
            NoteSound.Play();
        }
	}
}
