﻿using UnityEngine;

public class PlayerCollision : MonoBehaviour {

public PlayerMovement movment;

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Obstacle")
        {
            Debug.Log("An obstacle has been hit");
        }
    }
}
