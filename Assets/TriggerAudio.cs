﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAudio : MonoBehaviour {

    public AudioSource NoteSound;
    public AudioClip myClip;
    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter()
    {
    NoteSound.PlayOneShot(myClip);
    }
}
