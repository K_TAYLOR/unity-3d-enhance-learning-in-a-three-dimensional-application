A project created to explore if three-dimensional technology can make learning stimulating 
through cognitive behaviour, spatial awareness, moods and other factors. 
Focused on the theory of music presented to enhance learning. 

•	It features an interactive piano with the purpose of teaching the basics of music theory.

•	I created a 3D Virtual environment that could be navigated with game controls.

•	Various scripts were implemented to aid the play style of the application 
    such as the activation of sound, movement, mouse tracking, first-person controls, interactive text and more.
    
•	I had also created a fully functional menu system, along with a quit button to exit the application.



Created in Unity Game Engine, as a playable, interactive application/game which includes
the following implementations below:

menu system, player movement, camera implementation, free look, dynamic text, 
collision detection, map building, scripts etc.
